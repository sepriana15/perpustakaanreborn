<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\buku;

use App\Http\Controllers\Controller;

class bukuperpusController extends Controller
{
    //
        // public function home(){
        //     return view('index');
        // }

        // public function about(){
        //     return view('about', ['nama'=> 'Sepriana']);
        // }


    public function index(){
        //mengambil data dari tabel perpus
        // $perpus = DB::table('perpus')->paginate(5);
        
        // $perpus = DB::table('perpus')->get();

        $perpus = DB::table('buku')->paginate(5);

        // //memperlihatkan database
        // dump($perpus);

        return view('/buku/index', ['bookperpus' => $perpus]);
        // return view('buku.index', ['perpus' => $perpus]);
    
    }

    public function tambah(){
        return view('/buku/tambah');
    }

    public function store(Request $request){
        $request->validate([
            'nama_buku' => 'required',
            'penerbit' => 'required'
        ]);


        DB::create([
            'nama_buku' => $request->nama_buku,
            'id_buku' => $request->id_buku,
            'penerbit' => $request->penerbit,
            'kategori' => $request->kategori
        ]);
        return redirect('/buku')->with('status', 'Data berhasil ditambahkan');
    }

    //edit buku
    public function edit($id){
        $buku = DB::table('buku')->where('id_buku', $id)->get();

        return view('/buku/edit', ['buku' =>$buku]);
    }

    //update buku
    public function update(Request $request){
        DB::table('buku')->where('id_buku', $request->id)->update([
            'nama_buku' => $request->nama_buku,
            'penerbit' => $request->penerbit,
            'id_buku' => $request->id_buku,
            'kategori' => $request->kategori
        ]);

        return redirect('/buku');
    }

    //hapus buku
    public function hapus($id){
        DB::table('buku')->where('id_buku', $id)->delete();

        return redirect('/buku');
    }



}
