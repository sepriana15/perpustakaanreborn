<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\buku;

class buku extends Model
{
    //
    protected $table = 'buku';
    protected $fillable = ['nama', 'id', 'penerbit', 'kategori', 'kategori_id'];

    // public function databukuperpus(){
    //     // return $this -> hasMany('App\kategori');
    //     return $this -> belongsToMany(databukuperpus::class)->withPivot(['id_kategori']);
    // }

    // public function buku(){
    //     return $this->belongsTo(kategori::class);
    // }

    public function kategori(){
        return $this->hasMany(kategori::class);
    }
}
