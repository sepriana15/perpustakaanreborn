<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    //

    protected $table = 'kategori';

    protected $fillable = ['nama', 'id'];

    // public function buku(){
    //     return $this->hasMany(buku::class);
    // }

    public function databukuperpus(){
        // return $this -> hasMany('App\kategori');
        return $this -> belongsToMany(databukuperpus::class)->withPivot(['kategori_id']);
    }

    public function kategori(){
        return $this->belongsTo(kategori::class);
    }
}
