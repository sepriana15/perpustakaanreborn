<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
    $nama = 'Sepriana';
    return view('about', ['nama'=> $nama]);
});

Route::get('/buku', 'bukuperpusController@index');

Route::get('/about', 'bukuperpusController@about');

Route::get('/buku/tambah', 'bukuperpusController@tambah');

Route::get('databukuperpus', 'WebController@index');

Route::get('/buku/edit/{id}', 'bukuperpusController@edit');

Route::post('/buku/store', 'bukuperpusController@store');

Route::post('/buku/update', 'bukuperpusController@update');

Route::get('/buku/hapus/{id}', 'bukuperpusController@hapus');