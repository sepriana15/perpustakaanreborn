@extends('layout.main');

@section('title', 'Edit Buku')

@section('container')

<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Edit Buku</h1>

        <body>
            <a href="/buku">Kembali</a>

            <br/>
            <br/>

            @foreach($buku as $p)

            <form action="/buku/update" method="post" class="was-validated">
                {{ csrf_field() }}
                <!-- <input type="hidden" name="id" value="{{$p->id_buku}}"><br/> -->
                <div class="mb-3">
                    <label for="validationTextarea">Nama Buku</label>
                        <input type="text" textarea class="form-control is-invalid" id="validationTextarea" placeholder="Required example textarea" required name="nama_buku" value="{{$p->nama_buku}}"></textarea>
                    <div class="invalid-feedback">
                         <!-- Please enter nama buku in the textarea. -->
                    </div>
                </div>

                <div class="mb-3">
                    <label for="validationTextarea">Penerbit</label>
                        <input type="text" textarea class="form-control is-invalid" id="validationTextarea" placeholder="Required example textarea" required name="penerbit" value="{{$p->penerbit}}"></textarea>
                    <div class="invalid-feedback">
                         <!-- Please enter penerbit buku in the textarea. -->
                    </div>
                </div>

                <div class="mb-3">
                    <label for="validationTextarea">Id Buku</label>
                        <input type="int" textarea class="form-control is-invalid" id="validationTextarea" placeholder="Required example textarea" required name="id_buku" value="{{$p->id_buku}}"></textarea>
                    <div class="invalid-feedback">
                         <!-- Please enter id buku in the textarea. -->
                    </div>
                </div>

                <div class="mb-3">
                    <select class="custom-select" required name="kategori_id" value="{{$p->kategori}}">
                        <option value=" ">Kategori...</option>
                        <option value="1">Pemrograman</option>
                        <option value="2">Sastra</option>
                        <option value="3">Seni</option>
                    </select>
                        <div class="invalid-feedback">Example invalid custom select feedback</div>
                </div>
                <input type="submit" value="Simpan Data">

            </form>
              
        </body>
        @endforeach

    @endsection