@extends('layout.main');

@section('title', 'Daftar Buku')

@section('container')

<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Daftar Buku</h1>

                <style type="text/css">
                    .pagination li{
                        float: left;
                        list-style-type: none;
                        margin: 5px;
                    }
                </style>

            <br/>
            <a href="/buku/tambah">Tambah Buku</a>
            <br/>
            <br/>

            <table class="table">
                <thead class="thead-dark"></thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Buku</th>
                    <th scope="col">Id Buku</th>
                    <th scope="col">Penerbit</th>
                    <th scope="col">Kategori</th>
                    <th scope="col">Opsi</th>
                    </tr>
                </thead>

                <tbody>
                     @foreach($bookperpus as $book)

                    <tr>
                        <th scope="row">{{$loop->iteration }}</th>
                        <td>{{$book->nama_buku}}</td>
                        <td>{{$book->id_buku}}</td>
                        <td>{{$book->penerbit}}</td>
                        <td>{{$book->kategori}}</td>
                        <td>
                            <a href="/buku/edit/{{$book->id_buku}}" class="badge badge-success">Edit</a>
                            <a onclick="return confirm('Are you sure to delete this?')" href="/buku/hapus/{{$book->id_buku}}" class="badge badge-danger">Hapus</a>
                        </td>
                    </tr> 
                    @endforeach           
                </tbody>
            </table>

            <br/>

            {{$bookperpus -> links() }}
        </div>
    </div>
</div>
@endsection
