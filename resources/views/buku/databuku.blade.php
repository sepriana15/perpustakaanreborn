@extends('layout.main');

@section('title', 'Daftar Buku')

@section('container')

<div class="container">
    <div class="row">
        <div class="col-10">
            <h1 class="mt-3">Daftar Buku</h1>

            <table class="table">
                <thead class="thead-dark"></thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Buku</th>
                    <th scope="col">Id Buku</th>
                    <th scope="col">Penerbit</th>
                    <th scope="col">Kategori</th>
                    <th scope="col">Opsi</th>
                    </tr>
                </thead>

                <tbody>
                     @foreach( $perpus as $prps)

                    <tr>
                        <th scope="row">{{$loop->iteration }}</th>
                        <td>{{$prps->nama_buku}}</td>
                        <td>{{$prps->id_buku}}</td>
                        <td>{{$prps->penerbit}}</td>
                        <td>{{$prps->kategori}}</td>
                        <td>
                            <a href="" class="badge badge-success">Edit</a>
                            <a href="" class="badge badge-danger">Hapus</a>
                        </td>
                    </tr> 
                    @endforeach           
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
